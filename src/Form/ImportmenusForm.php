<?php
/**
 * @file
 * Contains \Drupal\dsfr_menu\Form\ImportmenusForm.
 */
namespace Drupal\dsfr_menu\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ImportmenusForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dsfr_menu_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get menus config

    $regions = \Drupal::service('dsfr_menu.config')->regionList();
    $label_menu = \Drupal::service('dsfr_menu.config')->labelMenus();

    // ------------------------------------------------------------------------------------------------------------ //
    // Get themes info
    
    $tools = \Drupal::service('dsfr_core.tools');
    $theming = $tools->checkTheme();
    $theme_current = $theming['current'];

    if( $theming['missing'] == true ) { $tools->msg($this->t('The DSFR theme does not seem to be present in your theme list.'), 'warning'); }
    
    // ------------------------------------------------------------------------------------------------------------ //
    // Check if the current theme has DSFR Regions

    $get_regions = $tools->checkRegions();
    $theme_regions = $get_regions['regions'];
    $message_region_not_found = $get_regions['not_found'];
    $region_not_found = false;
    $has_region = '';
    $options_attributes = [];

    foreach($regions as $key => $value) {

      $n = $key + 1;

      if( array_key_exists( $value, $theme_regions ) ) {

        $options[$n] =  $label_menu[$key];
        $has_region .= '/1';

      } else {

        $options[$n] =  $label_menu[$key].' (*)';
        $has_region .= '/0';
        $region_not_found = true;
      }
    }

    if( $region_not_found == true ) { $tools->msg($message_region_not_found, 'warning'); }

    foreach($label_menu as $k => $v) { 
      
      $id_clean = str_replace(' ', '-', strtolower($v));
      $id_clean = str_replace(' ', '', $id_clean);

      if( $id_clean == 'institutional-websites' ) { $id_clean = 'institutional'; }
      elseif( $id_clean == 'last-menu' ) { $id_clean = 'footer-last-menu'; }

      $blockEntityManagerExist = \Drupal::entityTypeManager()->getStorage('menu')->load($id_clean);

      if( $blockEntityManagerExist != null ) {
        
        $n = $k + 1;
        $options_attributes[$n] = ['disabled' => 'disabled'];
      } 
    }

    // ------------------------------------------------------------------------------------------------------------ //
    // Show form

    $form['options_menu'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the menus to import').' 
      ('.$this->t('automatically activated').'):',
      '#description' => '<br>'.$this->t('Each menu will have a factice item, 
      and the institutional menu will have links to institutions already supplied').'.',
      '#options' => $options,
      '#options_attributes' => $options_attributes
    ];
    $form['theme'] = [
      '#type' => 'hidden',
      '#value' => $theme_current
    ];
    $form['has_region'] = [
      '#type' => 'hidden',
      '#value' => $has_region
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get DSFR services
    $conf = \Drupal::service('dsfr_menu.config');
    $tools = \Drupal::service('dsfr_core.tools');

    // Get config
    $regions = $conf->regionList();
    $labels = $conf->labelMenus(); 
    $desc = $conf->description();
    $default_items = $conf->defaultItems();

    $log = [];

    if ( $form_state->getValue('options_menu') ) {

      $i = 0;
      foreach( $form_state->getValue('options_menu') as $n ) { $i = $i + $n;}

      if( $i > 0 ) {

        // ------------------------------------------------------------------------------------------------------------ //
        // Get results
        extract( $form_state->getValues() );
        $is_region = explode('/', $has_region);

        foreach ($options_menu as $key => $value) {

          $n = $key - 1;

          if( $value != 0 && $key == 1 ) { 
            $social_items = $conf->socialItems();
            $log[$key] = $conf->manageMenu('social', $labels[$n], $desc[$n], $social_items, $regions[$n], $is_region[$key], $theme); 
          
          } elseif( $value != 0 && $key > 1 && $key < 8) { // Footer (col 1 => 6)
            $i = strval($key-1); 
            $log[$key] = $conf->manageMenu('footer-top-'.$i, $this->t('Footer').' '.$i, $desc[$n], $default_items, 'footer_top_'.$i, $is_region[$key], $theme);
          
          } elseif( $value != 0 && $key == 8 ) { // Institutional
            $institutional_items = $conf->institutionalItems();
            $log[$key] = $conf->manageMenu('institutional', $labels[$n], $desc[$n], $institutional_items, $regions[$n], $is_region[$key], $theme, false); 

          } elseif( $value != 0 && $key == 9 ) { // Last menu
            $last_items = $conf->lastItems();
            $log[$key] = $conf->manageMenu('footer-last-menu', $labels[$n], $desc[$n], $last_items, $regions[$n], $is_region[$key], $theme); 
          }        
        }

      // ------------------------------------------------------------------------------------------------------------ //
      // Show results

        $error = $error2 = $nb_import = $nb_layout = 0;
        $log_message = $log_message2 = '';

        foreach($log as $row) {

          $nb_import += $row['import'];

          if( $row['already_menu'] != '') { 
            $log_message .= "<li>" . $row['already_menu'] . "</li>";
            $error++;
          }

          $nb_layout += $row['layout'];

          if( $row['already_block'] != '') { 
            $log_message2 .= "<li>" . $row['already_block'] . "</li>";
            $error2++;
          }
        }

        if( $nb_import > 0 ) { $tools->msg($this->t('Imported menu(s) !')); }
        if( $nb_layout > 0 ) { $tools->msg($this->t('Menu block(s) placed !')); }
        if( $error > 0) { 
          $log_message = $this->t('Menu(s) not imported, because already existing')."<ul> $log_message </ul>";      
          $tools->msg($log_message, 'warning'); }
        if( $error2 > 0) { 
          $log_message2= $this->t('Menu(s) already placed in their respective region')."<ul> $log_message2 </ul>";
          $tools->msg($log_message2, 'warning'); 
        }
      } else {

      $tools->msg('Please check at least one menu to import', 'error'); 

      }
    }
  }
}