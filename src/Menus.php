<?php

namespace Drupal\dsfr_menu;

use Drupal\Core\Controller\ControllerBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Provides route responses for the container info pages.
 */
class Menus extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function regionList() {
    return [ 
      "follow_social", 
      "footer_top_1", 
      "footer_top_2", 
      "footer_top_3", 
      "footer_top_4", 
      "footer_top_5",
      "footer_top_6",
      "footer_menu_first",
      "footer_menu_last",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function labelMenus() {
    return [
      $this->t('Social'),
      $this->t('Footer Top ').'1',
      $this->t('Footer Top ').'2',
      $this->t('Footer Top ').'3',
      $this->t('Footer Top ').'4',
      $this->t('Footer Top ').'5',
      $this->t('Footer Top ').'6',
      $this->t('Institutional websites'),
      $this->t('Last menu'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function description() {

    $desc= $this->t('Provides a menu for a footer column');
    return [
      $this->t('Provides a social menu'),
      $desc,
      $desc,
      $desc,
      $desc,
      $desc,
      $desc,
      $this->t('Provides an institutional - mandatory - menu (footer)'),
      $this->t('Provides a menu - mandatory - of legal requirements (footer)'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultItems() {
    return ['Item 1', 'Item 2'];
  }  

  /**
   * {@inheritdoc}
   */
  public function socialItems() {
    return [
      'Facebook', 
      'Twitter-X', 
      'Instagram', 
      'Linkedin',  
      'Youtube',
      //'RSS',
      'Mastodon',
      'Slack',
      'Telegram',
      'Tiktok',
      'Dailymotion',
      'Github',
      'Snapchat',
      'Twitch',
      'Threads',
      //'Whatsapp',
      'Vimeo'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function institutionalItems() {
    return [
      [
        'label' => 'legifrance.gouv.fr',
        'url'   => 'https://www.legifrance.gouv.fr/'
      ],
      [
        'label' => 'service-public.fr',
        'url'   => 'https://www.service-public.fr/'
      ],
      [
        'label' => 'numerique.gouv.fr',
        'url'   => 'https://www.numerique.gouv.fr/'
      ],
      [
        'label' => 'data.gouv.fr',
        'url'   => 'https://www.data.gouv.fr/fr/'
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function lastItems() {
    return [
      $this->t('Sitemap'),
      $this->t('Accessibility'),
      $this->t('Legal mention'),
      $this->t('Personal data'),
      $this->t('Cookies management')
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createItems($items, $id, $internal) {

    $menu_items = [];

    foreach($items as $item) {

      if( $internal == true ) {
        
        $title = $item;
        $link = ['uri' => 'internal:#'];

      } else {

        $title = $item['label'];
        $link = ['uri' => $item['url'] ];
      }

      $menu_items[] = [
        'title' => $title,
        'link' => $link,
        'menu_name' => $id,
      ];
    }
    return $menu_items;
  } 

  /**
   * {@inheritdoc}
   */
  public function createMenu(
    $id, 
    $label,
    $desc, 
    $items,
    $region, 
    $is_region, 
    $theme) {

    $get_menu = \Drupal\system\Entity\Menu::load($id);

    $log = [
      'import' => 0,
      'already_menu' => '',
      'layout' => 0,
      'already_block' => '',
    ];

    // --------------------------------------------------------------------------------------------------- //
    // STEP 1 //
    // Creation of the menu if the id does not exist
    if ( $get_menu == null ) {

      $log['import'] = 1;

      \Drupal::entityTypeManager()
      ->getStorage('menu')
      ->create([
        'id' => $id,
        'label' => $label,
        'description' => $desc,
      ])
      ->save();

      foreach ($items as $key => $row) { MenuLinkContent::create($items[$key])->save(); }

    } else { $log['already_menu'] = $label; }

    // --------------------------------------------------------------------------------------------------- //
    // STEP 2 //
    // The menu is placed in the block layout (disabled by default)
    if( $is_region == '1' ) {
      
      $id_menu_clean = str_replace('-', '_', $id);
      $id_menu_clean.= '_' . $theme;

      // Check if the ID block does not already exist
      $blockEntityManagerExist = [];
      $blockEntityManagerExist = \Drupal::entityTypeManager()->getStorage('block')->load($id_menu_clean);

      if( $blockEntityManagerExist == null ) {

        $log['layout'] = 1;

        $blockEntityManager = \Drupal::entityTypeManager()->getStorage('block');
        $block = $blockEntityManager->create(
          [
            'id'=> $id_menu_clean,
            'plugin' => 'system_menu_block:'.$id,
            'theme' => $theme,
            'status' => true
          ]
        );
        $block->setRegion($region);
        $block->save();

      } else { $log['already_block'] = $label ; }
    } 

    return $log;
  }  

  /**
   * {@inheritdoc}
   */
  public function manageMenu(
    $id, 
    $label, 
    $desc, 
    $items, 
    $region, 
    $is_region, 
    $theme = 'dsfr', 
    $internal = true) {

    $items_menu = $this->createItems($items, $id, $internal);
    $log = $this->createMenu($id, $label, $desc, $items_menu, $region, $is_region, $theme); 
    return $log;
  }

}
