# DSFR Menus

## Installation

### 1 - GET "form_options_attributes" and "dsfr_core"

https://www.drupal.org/project/form_options_attributes

```shell
composer require 'drupal/form_options_attributes:^2.0'
```

### 2 - Activate the modules

```shell
drush en dsfr_core form_options_attributes dsfr_menu -y
```

## Author

Code created by Jérôme Bouquet (https://www.drupal.org/u/netprogfr)